package jettijam.touchzoomview;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

public class TouchZoomViewActivity extends AppCompatActivity {
    Bitmap mBitmap;
    ImageView mImageView;

    int mTouchCount = 0;
    ArrayList<float[]> mTouchList = new ArrayList<>();

    int x0, y0;
    int px, py;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_touch_zoom_view);
        Intent intent = getIntent();
        String path = intent.getStringExtra("path");

        mBitmap = BitmapFactory.decodeFile(path);

        mImageView = (ImageView)findViewById(R.id.touchzoom_image_imageview);
        mImageView.setImageBitmap(mBitmap);
        mImageView.setScaleType(ImageView.ScaleType.MATRIX);
        mImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    float[] touch = new float[]{event.getX(), event.getY()};
                    mTouchList.add(touch);
                    mTouchCount ++;
                    if(mTouchCount == 2){
                        translate();
                        zoom();
                        mTouchCount = 0;
                        mTouchList.clear();
                    }

                }
                return false;
            }
        });



    }

    private void translate(){
        float x3 = 0, y3 = 0;
        for(float[] touch : mTouchList){
            x3 += touch[0];
            y3 += touch[1];
        }
        x3 /= 2f;
        y3 /= 2f;

//        Log.d("Translate", "X3, Y3 : "+x3+" , "+y3);

        float dx, dy;
        dx = x3 - x0;
        dy = y3 - y0;

//        Log.d("Translate", "DX, DY : "+dx+" , "+dy);

        float x, y;
        x = -dx;
        y = -dy;

        Matrix mat = new Matrix();
        mat.postTranslate(dx, dy);
        mImageView.setImageMatrix(mat);
//        mImageView.setX(x);
//        mImageView.setY(y);

        px = Math.round(x3);
        py = Math.round(y3);

//        Log.d("Translate", "X, Y : "+(x)+" , "+(y));
    }

    private void zoom(){
        int w, h;
        w = mImageView.getMeasuredWidth();
        h = mImageView.getMeasuredHeight();

        float w1, h1;

        for(float[] touch : mTouchList){
            Log.d("TOUCHLIST", "TOUCH X, Y : "+touch[0]+" , "+touch[1]);
        }

        w1 = mTouchList.get(1)[0]-mTouchList.get(0)[0];
        h1 = mTouchList.get(1)[1]-mTouchList.get(1)[1];

        float z_w, z_h, z;
        z_w = w/w1;
        z_h = h/h1;
        z = Math.min(z_w, z_h);
        Log.d("ZOOM", "Zw : "+z_w);
        Log.d("ZOOM", "Zh : "+z_h);
        Log.d("ZOOM", "Z : "+z);
        Matrix mat = new Matrix();
        mat.postScale(z, z, px, py);

        mImageView.setImageMatrix(mat);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        float x, y;
        int w, h;
        x = mImageView.getX();
        y = mImageView.getY();

        w = mImageView.getMeasuredWidth();
        h = mImageView.getMeasuredHeight();

        x0 = Math.round(x+w/2f);
        y0 = Math.round(y+h/2f);

//        Log.d("WindowFocusChanged", "X0, Y0 : "+x0+" , "+y0);
    }
}
